﻿using Microsoft.EntityFrameworkCore;
using Server_dot_NET.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server_dot_NET.DataLayer
{
    public class UniversityContext : DbContext
    {
        public UniversityContext(DbContextOptions dbContextOptions) : base(dbContextOptions)
        {

        }

        public virtual DbSet<StudentModel> Students { get; set; }

        public virtual DbSet<FacultyModel> Faculties { get; set; }

    }
}
