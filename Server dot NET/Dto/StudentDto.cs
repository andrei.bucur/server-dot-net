﻿namespace Server_dot_NET.Dto
{
    public class StudentDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int FacultyId { get; set; }
    }
}