﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server_dot_NET.Dto
{
    public class FacultyDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<StudentDto> Students { get; set; }
    }
}
