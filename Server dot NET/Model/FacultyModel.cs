﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Server_dot_NET.Model
{
    public class FacultyModel
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        public List<StudentModel> Students { get; set; }
    }
}
