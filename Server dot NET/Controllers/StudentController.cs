﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Server_dot_NET.DataLayer;
using Server_dot_NET.Dto;
using Server_dot_NET.Model;

namespace Server_dot_NET.Controllers
{
    [Route("students")]
    public class StudentController : Controller
    {
        private readonly UniversityContext _db;
        public StudentController(UniversityContext db)
        {
            _db = db;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var all = _db.Students.ToList();

            return Ok(all);
        }

        [HttpGet("{id}")]
        public IActionResult GetById([FromRoute] int id)
        {
            var studentById = _db.Students.Where(student => student.Id == id)
                .FirstOrDefault();

            return Ok(studentById);
        }

        [HttpPost]
        public IActionResult CreateNew([FromBody] StudentDto student)
        {
            _db.Students.Add(new StudentModel
            {
                FirstName = student.FirstName,
                LastName = student.LastName,
            });

            _db.SaveChanges();

            return Ok(new { Status = "Object Created" });
        }
    }
}
