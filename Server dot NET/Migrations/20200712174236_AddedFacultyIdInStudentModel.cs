﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Server_dot_NET.Migrations
{
    public partial class AddedFacultyIdInStudentModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FacultyId",
                table: "Students",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FacultyId",
                table: "Students");
        }
    }
}
